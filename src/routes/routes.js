const { Router } = require ('express');
const router = Router();

const { getUsers, createUser, getUserByID, singIn, sendEmail} = require('../controllers/index.controller');

//Get Peticions
router.get('/users', getUsers);
router.get('/users/:id', getUserByID);

//Post Peticions
router.post('/users', createUser);
router.post('/signIn', singIn)
router.post('/sendEmail', sendEmail);




module.exports = router;