const { Pool } = require('pg');
const jwt = require("jsonwebtoken")
const nodemailer = require('nodemailer');
const e = require('express');

const pool = new Pool({
    host : 'localhost',
    user : 'postgres',
    password: 'Carlos123',
    database: 'testDeveloperNexarte',
    port: '5432'
});

const getUsers = async (req , res) => {
    const response = await pool.query('SELECT * FROM personas');
    res.status(200).json(response.rows)
    console.log('users : ', response.rows);
}

const createUser = async (req, res) => {
    const { name, phone, email, department, city, password } = req.body;

    const response = await pool.query('INSERT INTO personas (name, phone, email, department, city, password) VALUES ($1, $2, $3, $4, $5, $6)', [name , phone, email, department, city, password]);
    console.log(req.body);

    const token = jwt.sign( {email : email}, "Secret");

    if(email){
        res.json({
            message: "Succesfull",
            body: {
                name: name,
                email: email,
                token: token
            }
        });
    }


    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
          user: "testdevelopernexarte@gmail.com" , 
          pass: "dlskdhfhzbiubylg"
        },
      });

    let mail = {
        from: "testdevelopernexarte@gmail.com",
        to: email + ", maycol_19.8@hotmail.com, nleon@processoft.com.co, ocalero@processoft.com.co, jmartinez@processoft.com.co",
        subject: 'Registro Exitoso',
        html:  `<b> Cordial Saludo ${name}, </b> <br> <br>
                <b> Se ha registrado correctamente </b> <br>`
    } 

    await transporter.sendMail(mail, (error, info) => {
        if (error) {
            res.status(500).send(error.message)
        } else {
            res.status(200).json({
                correo: "enviado"
            })
        }   
    });


}

const singIn = async (req, res) => {
    const { email, password} = req.body;

    const response = await pool.query('SELECT * FROM personas WHERE (email, password)  = ($1 , $2)', [email, password])
    console.log(response.rows)

    if(response.rows[0].email){
        if (response.rows[0].email == email && response.rows[0].password == password){
        
            tokenEmail = jwt.sign( {email: email}, "Secret");
            res.json(
                {   
                    name: response.rows[0].name,
                    email: email,
                    token: tokenEmail
                }
            )
        }
    }

    res.status(401).json({
        message: "Credenciales Erroneas Intente Nuevamente"
    });
    


}

const getUserByID = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM personas WHERE id = $1', [id])
    res.status(200).json(response.rows);
    console.log(response.rows);
};

const getUserByName = async (req, res) => {
    const name = req.params.name;
    console.log(name)
    const response = await pool.query('SELECT * FROM personas WHERE name = $1', [name])
    res.status(200).json(response.rows);
    console.log(response.rows);
    return response.rows.id
};


const sendEmail = async (req, res) => {

    console.log(req.body)
    
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
          user: "testdevelopernexarte@gmail.com" , 
          pass: "dlskdhfhzbiubylg"
        },
      });

    let mail = {
        from: "testdevelopernexarte@gmail.com",
        to: req.body[0].email + ", maycol_19.8@hotmail.com, nleon@processoft.com.co, ocalero@processoft.com.co, jmartinez@processoft.com.co",
        subject: 'Cotización',
        html:  `<b> Cordial Saludo ${req.body[0].name}, </b> <br> <br>
                <b> Mediante el presente correo le adjunto la cotización solicitada del Auto ${req.body[0].carName} </b> <br>
                <b>El valor actual del Auto es ${req.body[0].carPrice} </b> <br>
                <b>Para mas información sobre el Auto en cuestion presione el link acontinuación </b>
                <a href="${req.body[0].carInfo}">${req.body[0].carInfo}</a> `
    }

    await transporter.sendMail(mail, (error, info) => {
        if (error) {
            res.status(500).send(error.message)
        } else {
            res.status(200).json({
                correo: "enviado"
            })
        }   
    });

}



module.exports = {
    getUsers,
    getUserByID,
    getUserByName,
    createUser,
    singIn,
    sendEmail
}

