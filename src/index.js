const express = require('express');
const cors = require('cors')
const app = express();

//Settings
app.set('appName', 'Nexarte Backend');
app.set('port', 3000 );


//Middlewares
app.use(express.json());
app.use(cors())
app.use(express.urlencoded( {extended: false}));


// routes
app.use(require('./routes/routes'));


app.listen(app.get('port'), (req , res) => {
    console.log('estoy en el servidor', app.get('port'));
});